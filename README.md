# sipr_lab2_template

0.  
a. Wizualizacja w Rviz przedstawia robota i wybrane pozycje jego narzędzia.  
Strzałką żółtą oznaczono aktualną pozycję narzędzia robota.  
Strzałkami czerwonymi oznaczono punkty zadane do osiągnięcia w trakcie realizacji ćwiczenia laboratoryjnego - reprezentują one stany zapisane w zmiennych end_effector_state_1 oraz end_effector_state_2.  
Patrz instrukcja_obrazki/sipr_lab2_1.png  

b. Pierwszym celem ćwiczenia jest reazlizacja przejazdu manipulatora z położenia początkowego do położenia opisanego przez end_effector_state_1 z wykorzystaniem interpolacji we przestrzeni współrzędnych maszynowych (interpolacja "Joint" realizowana w punkcie 1. instrukcji).  

c. Drugim celem ćwiczenia jest realizacja przejazdu manipulatora z położenia opisanego przez end_effector_state_1 do położenia opisanego przez end_effector_state_2 z wykorzystaniem interpolacji liniowej w przestrzeni współrzędnych kartezjańskich (interpolacja "Linear" realizowana w 2., 3. i 4. punkcie instrukcji). W 2. punkcie instrukcji następuje wyznaczenie interpolowanych liniowo punktów przejściowych. W 3. punkcie instrukcji następuje realizacja ruchu w interpolacji "Linear" z wykorzystaniem kinematyki odwrotnej. W 4. punkcie instrukcji realizowana jest podobna funkcja, ale z wykorzystaniem Jakobianu.  

1. 
a. W funkcji publishJointJog wyznaczyć prędkość w stawach manipulatora w celu interpolacji we współrzędnych maszynowych. Najprościej osiągnąć ten efekt przez zadanie prędkości stałej w czasie, ale różnej dla każdeg stawu. W takiej sytuacji prędkość dla stawu może zależeć od położenia docelowego i położenia końcowego. Wcelu wyznaczenia różnicy kątowej wykorzystać funkcję angles::shortest_angular_distance(angle_1, angle_2).  

b. Po poprawnym zadaniu wartości manipulator wykona ruch w celu osiągnięcia pierwszego zadanego stanu opisanego przez end_effector_state_1.  
Patrz instrukcja_obrazki/sipr_lab2_2.png  

2. 
a. Poprawić implementację funkcji getPathPoint. Funkcja getPathPoint zwraca interpolowany liniowo punkt na ścieżce poniędzy dwoma pozycjami end_effector_state_1 i end_effector_state_2. Należy wziąć pod uwagę tylko translację - obroty nie zmieniają się pomiędzy tymi pozycjami.   
Po poprawnej implementacji zmiana aktualnej zadawanej pozycji interpolowanej liniowo będzie widoczna w Rviz za pomocą zielonej strzałki przemieszczającej się pomiedzy dwoma zadanymi położeniami (strzałki czerwone).  
Patrz instrukcja_obrazki/sipr_lab2_3.png oraz instrukcja_obrazki/sipr_lab2_4.png  

b. Poprawić funkcję checkIfPathSatisfiesBounds tak, aby umożliwiała sprawdzenie wszystkich punktów planowanej do wykonania trasy zrealizowanej jako interpolacja liniowa pomiędzy dwoma punktami przestrzeni kartezjańskiej. W tym celu wykorzystać funkcję chceckIfStateSatisfiesBounds oraz poprawioną funkcję getPathPoint. Poprawna implementacja checkIfPathSatisfiesBounds powinna pozwalać przejść do stanu control_state_ == 2.  

c. Sprawdzić czy dla stanów docelowych ustawionych przez setFailedIkGoals, zwracających błędy w trakcie wyznacznia kinematyki odwrotnej, następuje poprawne wykrycie tej sytuacji przez chceckIfStateSatisfiesBounds (komunikat "path not ok"). Dla stanów docelowych zadanych przez setOkIkGoals powinien wystąpić komunikat "path ok".  

3. 
a. W funkcji publishCartesianJogWithIk wyznaczyć w odpowiedni sposób błąd error do wykorzystania w celu regulacji prędkości. Błąd powinien określać różnicę pomiędzy aktualną, a zadaną wartością. Wcelu wyznaczenia różnicy kątowej wykorzystać funkcję angles::shortest_angular_distance(angle_1, angle_2).  
Patrz instrukcja_obrazki/sipr_lab2_3.png oraz instrukcja_obrazki/sipr_lab2_5.png  

b. Powinno to pozwolić na przejście do stanu control_state_ == 3 z wykorzystaniem publishCartesianJogWithIk.  
Patrz instrukcja_obrazki/sipr_lab2_3.png oraz instrukcja_obrazki/sipr_lab2_6.png  

4. 
a. W funkcji publishCartesianJogWithJacobian wyznaczyć delta_theta jako kierunek zmian współrzędnych kątowych w stawach robota. Wziąć pod uwagę tylko zmianę położenia, a zmianę współrzędnych orientacji przyjąć jako 0. W tym celu należy odpowiednio ustawić wektor delta_x na podstawie położenia aktualnego i położenia docelowego narzędzia manipulatora w układzie współrzędnych kartezjańskich, oraz wykorzystać wzór:  
delta_theta = I^(-1) * delta_x  
W celu odpowiedinego ustawienia wartości delta_x można wykorzystać metody Eigen::Vector3d::x(), Eigen::Vector3d::y(), Eigen::Vector3d::z().  
Patrz instrukcja_obrazki/sipr_lab2_3.png oraz instrukcja_obrazki/sipr_lab2_5.png  

b. Powinno to pozwolić na przejście do stanu control_state_ == 3 z wykorzystaniem publishCartesianJogWithJacobian.  
Patrz instrukcja_obrazki/sipr_lab2_3.png oraz instrukcja_obrazki/sipr_lab2_6.png  