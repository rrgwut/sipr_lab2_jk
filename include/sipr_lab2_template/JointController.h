/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   JointController.h
 * Author: maciek
 *
 * Created on 10 stycznia 2020, 14:35
 */

#ifndef JOINTCONTROLLER_H
#define JOINTCONTROLLER_H

#include "sipr_lab2_template/Pid.h"

#include <ros/ros.h>
#include <sipr_lab2_template/JointJog.h>
#include <sensor_msgs/JointState.h>
//#include <geometry_msgs/Point.h>
#include <nav_msgs/Path.h>
#include <control_toolbox/pid.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

class JointController 
{
public:
    explicit JointController();

    virtual ~JointController();
    
private:
    ros::NodeHandle nh_;
    ros::Subscriber sub_joint_states_;
    ros::Publisher pub_joint_jog_;
    ros::Publisher pub_path_;  
    ros::Publisher pub_goal_1_;
    ros::Publisher pub_goal_2_;
    ros::Publisher pub_goal_actual_;
    ros::Publisher pub_pose_actual_;
        
    robot_model_loader::RobotModelLoaderPtr robot_model_loader_;
    robot_model::RobotModelPtr kinematic_model_;
    robot_state::RobotStatePtr robot_state_;
    robot_state::JointModelGroup const* joint_model_group_ = nullptr;
    
    double max; 
    double min; 
    double Kp; 
    double Kd; 
    double Ki;
    std::vector< Pid > joint_pid_controllers;
    control_toolbox::Pid pid_controller_;
    
    double distance_tolerance;
    int control_state_;
    int path_steps;
    double path_tracking_velocity;
    std::string fixed_frame_id;
    double time_step;
    
    ros::Time time_now;
    
    Eigen::Affine3d end_effector_state_1, end_effector_state_2;
    std::vector< double > joint_values_start;
    
    double path_traversal_time;
    ros::Time path_tracking_start_time;
    
    void jointStatesCallback(sensor_msgs::JointState const& joint_states_msg);
    void setRandomPositions(Eigen::Affine3d &end_effector_state);
    std::vector< double > getJointValues(Eigen::Affine3d end_effector_state);
    double calcEuclideandDistanceTo(Eigen::Affine3d end_effector_state);
    
    Eigen::Vector3d getPathPoint(double t);
    Eigen::Affine3d getPathState(double t);
    Eigen::Vector3d getPathPoint(ros::Time time);
    Eigen::Affine3d getPathState(ros::Time time);
    void computePathTraversalTime();
    bool checkIfPathSatisfiesBounds();
    bool chceckIfStateSatisfiesBounds(Eigen::Affine3d end_effector_state);

    void publishPathMsg();
    void publishGoalsMsg();
    void publishGoalActualMsg(Eigen::Affine3d goal_actual);
    void publishActualPoseMsg(Eigen::Affine3d actual_pose);
    
    void setOkIkGoals();
    void setFailedIkGoals();
    void setGoal( Eigen::Affine3d &end_effector_goal, 
                  Eigen::Vector3d end_effector_translation,
                  Eigen::Matrix3d end_effector_rotation );
    void setRandomGoals();
    
    void switch_control_state(sensor_msgs::JointState const& joint_states_msg);
    void initPidControlers();
    void publishJointJog(std::vector<double> joint_goal_values);
    void publishJointJogStop();
    void publishCartesianJogWithIk(Eigen::Affine3d goal_state, sensor_msgs::JointState const& joint_states_msg);
    void publishCartesianJogWithJacobian(Eigen::Affine3d goal_state, sensor_msgs::JointState const& joint_states_msg);
};

#endif /* JOINTCONTROLLER_H */

