/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pid.h
 * Author: jan
 *
 * Created on January 14, 2020, 1:20 PM
 */

#ifndef PID_H
#define PID_H

class Pid
{
    public:
        Pid( double dt, double max, double min, double Kp, double Kd, double Ki );
        ~Pid();
//        double calculate( double setpoint, double pv );
        double calculate( double error );

    private:
        double _dt;
        double _max;
        double _min;
        double _Kp;
        double _Kd;
        double _Ki;
        double _pre_error;
        double _integral;
};

#endif /* PID_H */

