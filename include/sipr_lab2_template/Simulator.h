/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Simulator.h
 * Author: maciek
 *
 * Created on 10 stycznia 2020, 13:35
 */

#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <ros/ros.h>
#include <sipr_lab2_template/JointJog.h>
#include <sensor_msgs/JointState.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

class Simulator 
{
public:
    explicit Simulator();

    virtual ~Simulator();
    
    void update();
private:
    ros::NodeHandle nh_;
    ros::Subscriber sub_joint_jog_;
    ros::Publisher pub_joint_states_;
    
    sipr_lab2_template::JointJog joint_jog_msg_;
    
    robot_model_loader::RobotModelLoaderPtr robot_model_loader_;
    robot_model::RobotModelPtr kinematic_model_;
    robot_state::RobotStatePtr robot_state_;
    robot_state::JointModelGroup const* joint_model_group_ = nullptr;
    
    ros::Time prev_time_;
    bool time_set_ = false;

    void jointJogCallback(sipr_lab2_template::JointJog const& joint_jog_msg);
};

#endif /* SIMULATOR_H */

