/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   JointController.cpp
 * Author: maciek
 * 
 * Created on 10 stycznia 2020, 14:35
 */

#include "sipr_lab2_template/JointController.h"

#include <eigen_conversions/eigen_msg.h>
#include <angles/angles.h>


JointController::JointController() : nh_("~"), 
                                        control_state_(0), 
                                        path_tracking_velocity(0.05), 
                                        path_steps(1000), 
                                        fixed_frame_id("base_link"), 
                                        time_step(1.0/50),
                                        distance_tolerance(0.02)
{
    robot_model_loader_ = robot_model_loader::RobotModelLoaderPtr(
            new robot_model_loader::RobotModelLoader("robot_description"));
    
    kinematic_model_ = robot_model_loader_->getModel();
    
    ROS_INFO("Model frame: %s", kinematic_model_->getModelFrame().c_str());
    
    robot_state_ = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model_));
    robot_state_->setToDefaultValues();
    joint_model_group_ = robot_state_->getJointModelGroup("manipulator");
    
    pub_joint_jog_ = nh_.advertise<sipr_lab2_template::JointJog>("/joint_jog",1);
    sub_joint_states_ = nh_.subscribe("/joint_states", 1, &JointController::jointStatesCallback, this);
    
    pub_goal_1_ = nh_.advertise<geometry_msgs::PoseStamped>("/goal_1", 1);
    pub_goal_2_ = nh_.advertise<geometry_msgs::PoseStamped>("/goal_2", 1);
    pub_goal_actual_ = nh_.advertise<geometry_msgs::PoseStamped>("/goal_actual", 1);
    pub_pose_actual_ = nh_.advertise<geometry_msgs::PoseStamped>("/pose_actual", 1);
//    pub_path_ = nh_.advertise<nav_msgs::Path>("/path", 1);
}


void JointController::publishGoalsMsg()
{
    geometry_msgs::PoseStamped goal;
    goal.header.frame_id = fixed_frame_id;
    goal.header.stamp = ros::Time::now();
    
    tf::poseEigenToMsg (end_effector_state_1, goal.pose);
    pub_goal_1_.publish(goal);
    
    tf::poseEigenToMsg (end_effector_state_2, goal.pose);
    pub_goal_2_.publish(goal);
}


void JointController::publishGoalActualMsg(Eigen::Affine3d goal_actual)
{
    geometry_msgs::PoseStamped goal;
    goal.header.frame_id = fixed_frame_id;
    goal.header.stamp = ros::Time::now();
    
    tf::poseEigenToMsg (goal_actual, goal.pose);
    
    pub_goal_actual_.publish(goal);
}

void JointController::publishActualPoseMsg(Eigen::Affine3d actual_pose)
{
    geometry_msgs::PoseStamped goal;
    goal.header.frame_id = fixed_frame_id;
    goal.header.stamp = ros::Time::now();
    
    tf::poseEigenToMsg (actual_pose, goal.pose);
    
    pub_pose_actual_.publish(goal);
}

//void JointController::publishPathMsg()
//{
//    nav_msgs::Path path;
//    path.header.frame_id = fixed_frame_id;
//    path.header.stamp = path_tracking_start_time;
//    
//    geometry_msgs::PoseStamped pose;
//    pose.header.frame_id = fixed_frame_id;
//    
//    for(double time = 0.0; time < path_traversal_time; time+=time_step)
//    {        
//        pose.header.stamp = path_tracking_start_time + ros::Duration(time);
//        geometry_msgs::Pose2D pose_2d = getPathPose(pose.header.stamp);
//        pose.pose.position.x = pose_2d.x;
//        pose.pose.position.y = pose_2d.y;
//        pose.pose.orientation = tf::createQuaternionMsgFromYaw(pose_2d.theta);
//        
//        path.poses.push_back(pose);
//    }
//    
//    pub_path_.publish(path);
//}


JointController::~JointController() 
{
    
}

void JointController::setRandomPositions(Eigen::Affine3d &end_effector_state)
{
    const robot_state::JointModelGroup* joint_model_group = kinematic_model_->getJointModelGroup("manipulator");
    const std::vector<std::string>& joint_names = kinematic_model_->getVariableNames();
    
    robot_state_->setToRandomPositions(joint_model_group);
    end_effector_state = robot_state_->getGlobalLinkTransform("tool0");

//    /* Print end-effector pose. Remember that this is in the model frame */
//    ROS_INFO_STREAM("Translation: \n" << end_effector_state.translation() << "\n");
//    ROS_INFO_STREAM("Rotation: \n" << end_effector_state.rotation() << "\n");

    // Get Joint Values
    // ^^^^^^^^^^^^^^^^
    // We can retreive the current set of joint values stored in the state for the Panda arm.
    std::vector<double> joint_values;
    robot_state_->copyJointGroupPositions(joint_model_group, joint_values);
//    for (std::size_t i = 0; i < joint_names.size(); ++i)
//    {
//      ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
//    }  
}

std::vector<double> JointController::getJointValues(Eigen::Affine3d end_effector_state)
{
    const robot_state::JointModelGroup* joint_model_group = kinematic_model_->getJointModelGroup("manipulator");
    
    std::vector<double> joint_values;
    // Inverse Kinematics
    std::size_t attempts = 10;
    double timeout = 0.1;
    bool found_ik = robot_state_->setFromIK(joint_model_group, end_effector_state, attempts, timeout);

    // Now, we can print out the IK solution (if found):
    if (found_ik)
    {
//      ROS_INFO("Found IK solution");
      robot_state_->copyJointGroupPositions(joint_model_group, joint_values);
//          for (std::size_t i = 0; i < joint_names.size(); ++i)
//          {
//            ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
//          }
    }
    else
    {
      ROS_INFO("Did not find IK solution");
    }
    return joint_values;
}

bool JointController::chceckIfStateSatisfiesBounds(Eigen::Affine3d end_effector_state)
{
    const robot_state::JointModelGroup* joint_model_group = kinematic_model_->getJointModelGroup("manipulator");
    
    std::vector<double> joint_values;
    // Inverse Kinematics
    std::size_t attempts = 10;
    double timeout = 0.1;
    bool found_ik = robot_state_->setFromIK(joint_model_group, end_effector_state, attempts, timeout);

    // Now, we can print out the IK solution (if found):
    if (found_ik)
    {
//      ROS_INFO("Found IK solution");
      robot_state_->copyJointGroupPositions(joint_model_group, joint_values);
//          for (std::size_t i = 0; i < joint_names.size(); ++i)
//          {
//            ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
//          }
      return robot_state_->satisfiesBounds();
    }
    else
    {
      ROS_INFO("Did not find IK solution");
      return false;
    }
//    return found_ik;
}

Eigen::Vector3d JointController::getPathPoint(double t) 
{
    Eigen::Vector3d point;
    
    Eigen::Vector3d translacja_1 =  end_effector_state_1.translation();
    Eigen::Vector3d translacja_2 =  end_effector_state_2.translation();
    
//    point = ??;
    point << 0.0, 0.0, 0.0;
    
    return point;
}

Eigen::Affine3d JointController::getPathState(double t)
{
    Eigen::Vector3d point = getPathPoint(t);
    
    Eigen::Affine3d end_efector_state;
    end_efector_state.setIdentity();   // Set to Identity to make bottom row of Matrix 0,0,0,1
    end_efector_state.matrix().block<3,3>(0,0) = end_effector_state_1.rotation();
    end_efector_state.matrix().block<3,1>(0,3) = point;
    
    return end_efector_state;    
}

Eigen::Vector3d JointController::getPathPoint(ros::Time time)
{
    ros::Duration d = (time - path_tracking_start_time);
    double t = d.toSec() / path_traversal_time;
        
//    ROS_INFO_STREAM("Actual path param: " << t);
    Eigen::Vector3d point = getPathPoint(t);
        
    return point;    
}

Eigen::Affine3d JointController::getPathState(ros::Time time)
{
    ros::Duration d = (time - path_tracking_start_time);
    double t = d.toSec() / path_traversal_time;
        
//    ROS_INFO_STREAM("Actual path param: " << t);
    Eigen::Affine3d state = getPathState(t);
        
    return state;    
}

double calcEuclideandDistance(Eigen::Vector3d p1, Eigen::Vector3d p2)
{
    double distance = (p1-p2).norm();
}

void JointController::computePathTraversalTime()
{
    double distance = calcEuclideandDistance(end_effector_state_1.translation(), end_effector_state_2.translation());
    path_traversal_time = distance / path_tracking_velocity;
    
    ROS_INFO("Estimated path traversal time: %f", path_traversal_time);
}

bool JointController::checkIfPathSatisfiesBounds()
{
    for(int i=0; i<=path_steps; i++)
    {
        Eigen::Affine3d state = getPathState( double(i)/path_steps );
        
        //??
    }
    return true;
}

double JointController::calcEuclideandDistanceTo(Eigen::Affine3d end_effector_state)
{
    const robot_state::JointModelGroup* joint_model_group = kinematic_model_->getJointModelGroup("manipulator");
    
    // Forward Kinematics
    // ^^^^^^^^^^^^^^^^^^
    const Eigen::Affine3d& actual_end_effector_state = robot_state_->getGlobalLinkTransform("tool0");
    
    double distance = calcEuclideandDistance(end_effector_state.translation(), actual_end_effector_state.translation());
    return distance;
}

Eigen::MatrixXd pseudoInverse(const Eigen::MatrixXd& u_matrix, const Eigen::MatrixXd& v_matrix, const Eigen::MatrixXd& s_diagonals)
{
  return v_matrix * s_diagonals.inverse() * u_matrix.transpose();
}

void JointController::setFailedIkGoals()
{
    //sciezka bez rozwiazania IK
    Eigen::Vector3d end_effector_state_1_translation(0.260686, -0.458277, 0.62705);
    Eigen::Matrix3d end_effector_state_1_rotation;
    end_effector_state_1_rotation << 0.556082,  0.456804, 0.694337,
                                    -0.636835,  0.770995, 0.00279304,
                                    -0.534054, -0.443731, 0.719645;


    Eigen::Vector3d end_effector_state_2_translation(-1.41332, -0.122095, 0.429809);
    Eigen::Matrix3d end_effector_state_2_rotation;
    end_effector_state_2_rotation << 0.556082,  0.456804, 0.694337,
                                    -0.636835,  0.770995, 0.00279304,
                                    -0.534054, -0.443731, 0.719645;
    
    
    setGoal( end_effector_state_1, end_effector_state_1_translation, end_effector_state_1_rotation);
    setGoal( end_effector_state_2, end_effector_state_2_translation, end_effector_state_2_rotation);
}

void JointController::setOkIkGoals()
{
    //sciezka z rozwiazaniem IK
    Eigen::Vector3d end_effector_state_1_translation(1.18655, -0.405595, 0.339903);
    Eigen::Matrix3d end_effector_state_1_rotation;
    end_effector_state_1_rotation << 0.227001, 0.960308, -0.16211,
                                    -0.891158, 0.271958,  0.363147,
                                     0.39282,  0.0620312, 0.917521;


    Eigen::Vector3d end_effector_state_2_translation(0.0050048, -0.392993, -0.611282);
    Eigen::Matrix3d end_effector_state_2_rotation;
    end_effector_state_2_rotation << 0.227001, 0.960308, -0.16211,
                                    -0.891158, 0.271958,  0.363147,
                                     0.39282,  0.0620312, 0.917521;
    
    
    setGoal( end_effector_state_1, end_effector_state_1_translation, end_effector_state_1_rotation);
    setGoal( end_effector_state_2, end_effector_state_2_translation, end_effector_state_2_rotation);
}

void JointController::setRandomGoals()
{
    setRandomPositions(end_effector_state_1);
    setRandomPositions(end_effector_state_2);
}

void JointController::setGoal( Eigen::Affine3d &end_effector_goal, 
                               Eigen::Vector3d end_effector_translation,
                               Eigen::Matrix3d end_effector_rotation)
{
    end_effector_goal.matrix().block<3,1>(0,3) = end_effector_translation;
    end_effector_goal.matrix().block<3,3>(0,0) = end_effector_rotation;
    
    /* Print end-effector pose. Remember that this is in the model frame */
    ROS_INFO_STREAM("Goal translation: \n" << end_effector_goal.translation());
    ROS_INFO_STREAM("Goal rotation: \n" << end_effector_goal.rotation());
}

void JointController::initPidControlers()
{
    int n = kinematic_model_->getVariableCount();
    
    max = 1.0; 
    min = -1.0; 
    Kp = 1.0; 
    Kd = 0.0; 
    Ki = 0.0;

    for(int i = 0; i < n; i++)
    {
        Pid joint_pid = Pid( time_step, max, min, Kp, Kd, Ki );
        joint_pid_controllers.push_back(joint_pid);
    }
}

void JointController::switch_control_state(sensor_msgs::JointState const& joint_states_msg)
{
    if(control_state_ == 0)//set start/end linear interpolation points
    {
        initPidControlers();
                
        control_state_ = 1;
        ROS_INFO("control_state_ == 1");
    }
    
    if(control_state_ == 1)//go to start point with joint interpolation
    {
        robot_state_->setVariableValues(joint_states_msg);
        double distance = calcEuclideandDistanceTo(end_effector_state_1);
        if( distance < distance_tolerance )
        {
            publishJointJogStop();
            
            bool path_ok = checkIfPathSatisfiesBounds();
            if(path_ok)
            {
                ROS_INFO("path ok");

                control_state_ = 2;
                ROS_INFO("control_state_ == 2");
                
                path_tracking_start_time = time_now;
                computePathTraversalTime();
            }
            else
            {
                ROS_INFO("path not ok");
                ros::shutdown();
            }
            
        }
    }
    
    if(control_state_ == 2 )
    {
        robot_state_->setVariableValues(joint_states_msg);
        double goal_distance = calcEuclideandDistanceTo(end_effector_state_2);
        ros::Duration d = (time_now - path_tracking_start_time);
        if( goal_distance < distance_tolerance && d.toSec() >= path_traversal_time)
        {
            ROS_INFO("control_state_ == 3");
            control_state_ = 3;
        }
    }
}

void printJointValues(std::vector<double> joint_values, std::vector<std::string> joint_names)
{
    for (std::size_t i = 0; i < joint_names.size(); ++i)
    {
        ROS_INFO("Goal joint %s: %f", joint_names[i].c_str(), joint_values[i]);
    }
}

void JointController::publishJointJog(std::vector<double> joint_goal_values)
{
    int n = kinematic_model_->getVariableCount();
    
    sipr_lab2_template::JointJog joint_jog_msg;
    joint_jog_msg.header.stamp = time_now;
    joint_jog_msg.joint_names = kinematic_model_->getVariableNames();
    joint_jog_msg.velocities.resize(n,0.0);

    for(int i=0; i<n; i++)
    {        
//        double joint_error = joint_goal_values[i] - joint_states_msg.position[i];
        joint_jog_msg.velocities[i] = 0.0;//constant velocity
    }

    pub_joint_jog_.publish(joint_jog_msg);
}

void JointController::publishJointJogStop()
{
    int n = kinematic_model_->getVariableCount();
    
    sipr_lab2_template::JointJog joint_jog_msg;
    joint_jog_msg.header.stamp = time_now;
    joint_jog_msg.joint_names = kinematic_model_->getVariableNames();
    joint_jog_msg.velocities.resize(n,0.0);

    for(int i=0; i<n; i++)
    {        
        joint_jog_msg.velocities[i] = 0;
    }

    pub_joint_jog_.publish(joint_jog_msg);
}

void JointController::publishCartesianJogWithIk(Eigen::Affine3d goal_state, sensor_msgs::JointState const& joint_states_msg)
{
    int n = kinematic_model_->getVariableCount();
    
    std::vector<double> goal_joints = getJointValues(goal_state);

    sipr_lab2_template::JointJog joint_jog_msg;
    joint_jog_msg.header.stamp = time_now;
    joint_jog_msg.joint_names = kinematic_model_->getVariableNames();
    joint_jog_msg.velocities.resize(n,0.0);

    for(int i=0; i<n; i++)
    {        
        double error = 0.0;
        double output = joint_pid_controllers[i].calculate(error);
        joint_jog_msg.velocities[i] = output;
    }

    pub_joint_jog_.publish(joint_jog_msg); 
}

void JointController::publishCartesianJogWithJacobian(Eigen::Affine3d goal_state, sensor_msgs::JointState const& joint_states_msg)
{
    int n = kinematic_model_->getVariableCount();
    
    const Eigen::Affine3d& actual_end_effector_state = robot_state_->getGlobalLinkTransform("tool0");
    Eigen::Vector3d delta_goal = goal_state.translation() - actual_end_effector_state.translation();

    // Get the Jacobian
    robot_state_->setVariableValues(joint_states_msg);
    Eigen::MatrixXd jacobian = robot_state_->getJacobian(joint_model_group_);
//        ROS_INFO_STREAM("Jacobian: \n" << jacobian << "\n");  

    Eigen::JacobiSVD<Eigen::MatrixXd> svd(jacobian, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Eigen::VectorXd delta_x(6);
    delta_x << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;//wpisać odpowiednie wartości
    Eigen::MatrixXd inverse_jacobian = pseudoInverse(svd.matrixU(), svd.matrixV(), svd.singularValues().asDiagonal());
    Eigen::VectorXd delta_theta(6);// = ??;
    delta_theta << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;//zastępczo

    //publish joint velocities
    sipr_lab2_template::JointJog joint_jog_msg;
    joint_jog_msg.header.stamp = time_now;
    joint_jog_msg.joint_names = kinematic_model_->getVariableNames();
    joint_jog_msg.velocities.resize(n,0.0);

    for(int i=0; i<n; i++)
    {        
        double error = delta_theta[i];
        double output = joint_pid_controllers[i].calculate(error);
        joint_jog_msg.velocities[i] = output;
    }

    pub_joint_jog_.publish(joint_jog_msg); 
}

void JointController::jointStatesCallback(sensor_msgs::JointState const& joint_states_msg)
{
    time_now = ros::Time::now();
//    ROS_INFO_STREAM("time_now: \t" << time_now);
    
    int n = kinematic_model_->getVariableCount();
    const std::vector<std::string>& joint_names = kinematic_model_->getVariableNames();
    const robot_state::JointModelGroup* joint_model_group = kinematic_model_->getJointModelGroup("manipulator");
    
    if(control_state_ == 0)//set start/end linear interpolation points
    {
        ROS_INFO("control_state_ == 0");
        
        setOkIkGoals();
//        setFailedIkGoals();
//        setRandomGoals()
        
        joint_values_start = joint_states_msg.position;
        
        switch_control_state(joint_states_msg);
    }
    
    if(control_state_ == 1)//go to start point with joint interpolation
    {
        std::vector<double> joint_values_1 = getJointValues(end_effector_state_1);
//        printJointValues(joint_values_1, joint_names);
        
        robot_state_->setVariableValues(joint_states_msg);
        
        publishJointJog(joint_values_1);
        
        switch_control_state(joint_states_msg);
    }
    
    if(control_state_ == 2)//go to end point with linear interpolation
    {
        Eigen::Affine3d actual_goal = getPathState(time_now);
        publishGoalActualMsg(actual_goal);
        
        robot_state_->setVariableValues(joint_states_msg);
        double goal_distance = calcEuclideandDistanceTo(end_effector_state_2);
        if( goal_distance > distance_tolerance )
        {
            publishCartesianJogWithIk(actual_goal, joint_states_msg);
//            publishCartesianJogWithJacobian(actual_goal, joint_states_msg);
        }
        else
        {
            publishJointJogStop();
        }
        
        switch_control_state(joint_states_msg);
    }
    
    if( control_state_ > 0 )
    {
        publishGoalsMsg();
    }
    robot_state_->setJointGroupPositions(joint_model_group_, joint_states_msg.position);
    const Eigen::Affine3d& actual_end_effector_state = robot_state_->getGlobalLinkTransform("tool0");
    publishActualPoseMsg(actual_end_effector_state);
    
    
    if( control_state_ == 3 )
    {
        ros::shutdown();
    }
}
