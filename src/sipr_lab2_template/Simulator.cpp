

#include "sipr_lab2_template/Simulator.h"

Simulator::Simulator() : nh_("~")
{
    robot_model_loader_ = robot_model_loader::RobotModelLoaderPtr(
            new robot_model_loader::RobotModelLoader("robot_description"));
    
    kinematic_model_ = robot_model_loader_->getModel();
    
    ROS_INFO("Model frame: %s", kinematic_model_->getModelFrame().c_str());
    
    robot_state_ = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model_));
    robot_state_->setToDefaultValues();
    joint_model_group_ = robot_state_->getJointModelGroup("manipulator");
    
    pub_joint_states_ = nh_.advertise<sensor_msgs::JointState>("/joint_states",1);
    sub_joint_jog_ = nh_.subscribe("/joint_jog", 1, &Simulator::jointJogCallback, this);
}


Simulator::~Simulator() 
{
    
}

void Simulator::update()
{
    ros::Time time_now = ros::Time::now();
    
    int n = kinematic_model_->getVariableCount();
    sensor_msgs::JointState joint_state_msg;
    joint_state_msg.header.stamp = time_now;
    joint_state_msg.name = kinematic_model_->getVariableNames();
    joint_state_msg.velocity = joint_jog_msg_.velocities;    
    robot_state_->copyJointGroupPositions(joint_model_group_, joint_state_msg.position);
    
    if(!time_set_)
    {
        prev_time_ = joint_state_msg.header.stamp;
        time_set_ = true;
    }
    
    if(joint_jog_msg_.velocities.size() == n)
    {
        double dt = (time_now - prev_time_).toSec();

        for(int i=0; i<n; i++)
        {        
            joint_state_msg.position[i] += dt*joint_state_msg.velocity[i];
        }
    }

    robot_state_->enforceBounds();
    if(robot_state_->satisfiesBounds())
    {
        robot_state_->setJointGroupPositions(joint_model_group_, joint_state_msg.position);
        pub_joint_states_.publish(joint_state_msg);
    }
    else
    {
        sensor_msgs::JointState joint_old_state_msg;
        joint_old_state_msg.header.stamp = time_now;
        joint_old_state_msg.name = kinematic_model_->getVariableNames();
        joint_old_state_msg.velocity = joint_jog_msg_.velocities;    
        robot_state_->copyJointGroupPositions(joint_model_group_, joint_old_state_msg.position);
        pub_joint_states_.publish(joint_old_state_msg);
        ROS_INFO("fail robot_state_ not satisfiesBounds()");
    }
//    robot_state_->setJointGroupPositions(joint_model_group_, joint_state_msg.position);
//    pub_joint_states_.publish(joint_state_msg);
    
    prev_time_ = time_now;
}

void Simulator::jointJogCallback(sipr_lab2_template::JointJog const& joint_jog_msg)
{
    joint_jog_msg_ = joint_jog_msg;
}
