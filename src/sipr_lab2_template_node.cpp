#include <cstdlib>
#include <sipr_lab2_template/JointController.h>
#include <sipr_lab2_template/Simulator.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    ros::init(argc, argv, "sipr_lab2_template_node");
    
    JointController controller;    
    Simulator simulator;
    
    ros::Rate loop_rate(50);
    
    while(ros::ok())
    {
        ros::spinOnce();        
        simulator.update();        
        loop_rate.sleep();
    }

    return 0;
}
