/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sipr_simulator_node.cpp
 * Author: maciek
 *
 * Created on 10 stycznia 2020, 13:25
 */

#include <cstdlib>
#include <sipr_lab2_template/Simulator.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    ros::init(argc, argv, "sipr_lab2_simulator");
    
    Simulator simulator;
    
    ros::Rate loop_rate(50);
    
    while(ros::ok())
    {
        ros::spinOnce();        
        simulator.update();        
        loop_rate.sleep();
    }

    return 0;
}

